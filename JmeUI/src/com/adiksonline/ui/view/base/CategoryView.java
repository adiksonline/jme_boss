/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui.view.base;

import com.nokia.mid.ui.CategoryBar;
import com.nokia.mid.ui.ElementListener;


/**
 *
 * @author ADIKSONLINE
 */
public abstract class CategoryView extends NativeView implements ElementListener {

  private CategoryBar catbar;

  public CategoryView() {
    this("Category view");
  }

  public CategoryView(String title) {
    super(title);
  }

  /**
   *
   * @param forward the value of forward
   */
  public void activate(boolean forward) {
    Viewable view = getSelectedView();
    // avoids unneccessary recursion
    if (this == view) {
      show();
    } else {
      view.activate(forward);
    }
    CategoryBar c = getCatBar();
    c.setVisibility(true);
  }

  private CategoryBar getCatBar() {
    if (catbar == null) {
      catbar = getCategoryBar();
      if (catbar == null) {
        throw new NullPointerException("Category bar cannot be null");
      }
      catbar.setElementListener(this);
    }
    return catbar;
  }

  /**
   *
   * @param forward the value of forward
   */
  public void deactivate(boolean forward) {
    getCatBar().setVisibility(false);
  }

  protected void revalidateCategoryBar() {
    CategoryBar categoryBar = getCategoryBar();
    if (categoryBar == null) {
      throw new NullPointerException("Category bar cannot be null");
    }
    catbar.setElementListener(null);
    catbar = categoryBar;
    catbar.setElementListener(this);
  }

  protected abstract CategoryBar getCategoryBar();

  protected abstract Viewable getSelectedView();
}
