/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui;

import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Image;

/**
 *
 * @author ADIKSONLINE
 */
public class NativeDialog extends AbsNativeDialog {

  public NativeDialog(String title) {
    super(title);
  }

  public NativeDialog(String title, String alertText, Image alertImage, AlertType alertType) {
    super(title, alertText, alertImage, alertType);
  }

  public NativeDialog(String title, String alertText, AlertType alertType) {
    super(title, alertText, null, alertType);
  }
  
}
