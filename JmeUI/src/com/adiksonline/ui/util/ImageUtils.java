/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui.util;

import java.io.IOException;
import javax.microedition.lcdui.Image;

/**
 *
 * @author Oladeji
 */
public class ImageUtils {

  public static Image loadImage(String path) {
    try {
      return Image.createImage(path);
    } catch (IOException ex) {
      ex.printStackTrace();
      return null;
    }
  }
}