/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui.util;

import com.adiksonline.ui.DisplayManager;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Font;

/**
 *
 * @author ADIKSONLINE
 */
public class StyleUtils {

  public static final int COLOR_BG;
  public static final int COLOR_FG;
  public static final int COLOR_GREY;
  public static final int COLOR_DGREY;
  public static final int COLOR_SELECT;
  public static final Font FONT_ITALIC = Font.getFont(Font.FACE_SYSTEM, Font.STYLE_ITALIC, Font.SIZE_SMALL);
  public static final Font FONT_BOLD = Font.getFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_SMALL);
  public static final Font FONT_NORMAL = Font.getFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_SMALL);
  public static final Font FONT_BBOLD = Font.getFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_MEDIUM);
  public static final Font FONT_DEFAULT = Font.getDefaultFont();

  static {
    // XXX: Consider using the @link{com.nokia.mid.theme} package instead
    Display d = DisplayManager.getNativeDisplay();
    COLOR_GREY = 0xa0a0a0;
    COLOR_DGREY = 0x505050;
    COLOR_BG = d.getColor(Display.COLOR_BACKGROUND);
    COLOR_FG = d.getColor(Display.COLOR_FOREGROUND);
    int temp = d.getColor(Display.COLOR_HIGHLIGHTED_BACKGROUND);
    if (temp == COLOR_BG) {
      temp = COLOR_FG;
    }
    COLOR_SELECT = temp;
  }
}
