/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui;

import com.sun.lwuit.Button;
import com.sun.lwuit.Graphics;
import com.sun.lwuit.Image;

/**
 *
 * @author ADIKSONLINE
 */
public class ButtonBar extends Button {

  private final Image unselected;
  private final Image selected;
  private Image current;

  public ButtonBar(String text, Image unselected, Image selected) {
    super(text);
    this.unselected = unselected;
    this.selected = selected;
    setUIID("FlatButton");
  }

  public void paint(Graphics g) {
    current = (getState() == STATE_PRESSED) ? selected : unselected;
    super.paint(g);
    int x = getX();
    int y = getY();
    int rp = getStyle().getPadding(RIGHT);
    if (current != null) {
      x = x + getWidth() - rp - current.getWidth();
      y += getHeight() / 2 - current.getHeight() / 2;
      g.drawImage(current, x, y);
    }
  }
}
