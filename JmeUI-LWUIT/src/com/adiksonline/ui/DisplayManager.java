/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui;

import javax.microedition.lcdui.Displayable;
import javax.microedition.midlet.MIDlet;

/**
 *
 * @author ADIKSONLINE
 */
public class DisplayManager extends AbsDisplayManager {

  private static Displayable lwuitDisplayable;

  public static void init(MIDlet m) {
    com.sun.lwuit.Display.init(m);
    AbsDisplayManager.init(m);
  }

  public static void setCurrent(com.sun.lwuit.Form f) {
    Displayable d = getLwuitDisplayable();
    if (getCurrent() == d) {
      return;
    }
    if (d == null) {
      throw new IllegalArgumentException("Unable to get displayable. LWUIT form has not been initialized on the screen");
    }
    setCurrent(d);
  }

  public static void setCurrent(AbsNativeDialog a, com.sun.lwuit.Form f) {
    a.next = getLwuitDisplayable();
    setCurrent(a);
  }

  /**
   * @return the lwuitDisplayable
   */
  public static Displayable getLwuitDisplayable() {
    return lwuitDisplayable;
  }

  /**
   * @param aLwuitDisplayable the lwuitDisplayable to set
   */
  public static void setLwuitDisplayable(Displayable aLwuitDisplayable) {
    lwuitDisplayable = aLwuitDisplayable;
  }
}
