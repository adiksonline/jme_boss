/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui.form;

import com.adiksonline.ui.DisplayManager;
import com.adiksonline.ui.util.StyleUtils;
import com.sun.lwuit.Display;
import com.sun.lwuit.Font;
import com.sun.lwuit.Form;
import com.sun.lwuit.Graphics;
import com.sun.lwuit.Image;
import com.sun.lwuit.Painter;
import com.sun.lwuit.geom.Rectangle;
import java.util.Timer;
import java.util.TimerTask;
import javax.microedition.lcdui.Displayable;

/**
 *
 * @author ADIKSONLINE
 */
public class SplashForm extends Form {

  private final Image splash;

  public SplashForm(Image splashImage, final Runnable doNext) {
    Display.getInstance().setForceFullScreen(true);
    if (splashImage == null) {
      splashImage = Image.createImage(64, 64, 0x0000ff);
    }
    splash = splashImage;
    setGlassPane(new SplashPainter());
    new Timer().schedule(new TimerTask() {
      public void run() {
        Display.getInstance().setForceFullScreen(false);
        Displayable d = DisplayManager.getVisible();
        DisplayManager.setLwuitDisplayable(d);
        DisplayManager.setCurrent(d);
        Display.getInstance().callSerially(doNext);
      }
    }, 2000);
  }

  private class SplashPainter implements Painter {

    public void paint(Graphics g, Rectangle r) {
      g.setColor(StyleUtils.COLOR_DGREY);
      //g.fillRect(0, 0, getWidth(), getHeight());
      int x = (getWidth() - splash.getWidth()) / 2;
      int y = (getHeight() - splash.getHeight()) / 2;
      g.drawImage(splash, x, y);
      final String text = Display.getInstance().getProperty("MIDlet-Name", "Application");
      final Font font = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_LARGE);
      x = (getWidth() - font.stringWidth(text)) / 2;
      y += splash.getHeight() + 5;
      g.setFont(font);
      g.setColor(StyleUtils.COLOR_GREY);
      g.drawString(text, x, y);
    }
  }
}
