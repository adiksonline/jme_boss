/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui.renderer;

import com.adiksonline.ui.FunListRenderer;
import com.adiksonline.ui.util.Strings;
import com.adiksonline.ui.util.StyleUtils;
import com.sun.lwuit.Component;
import com.sun.lwuit.Font;
import com.sun.lwuit.Graphics;
import com.sun.lwuit.geom.Dimension;
import java.util.Vector;

/**
 *
 * @author ADIKSONLINE
 */
public class MultilineRenderer extends Component implements FunListRenderer {

  private Vector lines;
  private String query;
  private static final Font font = StyleUtils.FONT_NORMAL;

  private void setItem(String query) {
    this.query = query;
  }

  private Vector getLines() {
    if (lines == null) {
      int width = getWidth();
      width -= getStyle().getMargin(LEFT) - getStyle().getMargin(RIGHT) - getSideGap();
      lines = Strings.splitToLines(query, font, width);
    }
    return lines;
  }

  public void paint(Graphics g) {
    int leftPadding = getStyle().getMargin(LEFT);
    int topPadding = getStyle().getMargin(TOP);
    g.setFont(font);
    g.setColor(StyleUtils.COLOR_FG);
    for (int i = 0; i < getLines().size(); i++) {
      g.drawString((String) lines.elementAt(i), getX() + leftPadding, getY() + topPadding);
      topPadding += font.getHeight();
    }
  }

  protected Dimension calcPreferredSize() {
    int height = getStyle().getMargin(TOP) + getStyle().getMargin(BOTTOM) + getLines().size() * font.getHeight();
    return new Dimension(50, height);
  }

  public Component renderItem(Component cmp, Object item) {
    MultilineRenderer renderer = new MultilineRenderer();
    renderer.setItem((String) item);
    return renderer;
  }
}
