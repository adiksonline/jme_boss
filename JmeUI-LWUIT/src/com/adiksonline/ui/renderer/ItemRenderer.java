/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui.renderer;

import com.adiksonline.ui.FunListRenderer;
import com.adiksonline.ui.util.ImageUtils;
import com.adiksonline.ui.util.Strings;
import com.adiksonline.ui.util.StyleUtils;
import com.sun.lwuit.Component;
import com.sun.lwuit.Graphics;
import com.sun.lwuit.Image;
import com.sun.lwuit.geom.Dimension;
import java.util.Vector;

/**
 *
 * @author ADIKSONLINE
 */
public class ItemRenderer extends Component implements FunListRenderer {

  private static final Image arrow = ImageUtils.loadImage("/other/hb_default.png");
  private Item item;
  private Vector lines;

  private void setItem(Item item) {
    this.item = item;
  }

  private Vector getLines() {
    if (lines == null) {
      int width = getWidth();
      width -= getStyle().getMargin(LEFT) + getStyle().getMargin(RIGHT) + getSideGap();
      if (item.clickable) {
        width -= arrow.getWidth() + 6;
      }
      lines = Strings.splitToLines(item.value, StyleUtils.FONT_NORMAL, width);
    }
    return lines;
  }

  public void paint(Graphics g) {
    int lp = getStyle().getMargin(LEFT);
    int tp = getStyle().getMargin(TOP);
    g.setFont(StyleUtils.FONT_BOLD);
    g.setColor(StyleUtils.COLOR_FG);
    g.drawString(item.title, getX() + lp, getY() + tp);
    tp += StyleUtils.FONT_BOLD.getHeight();
    if (item.clickable) {
      g.setColor(StyleUtils.COLOR_SELECT);
    } else {
      g.setColor(StyleUtils.COLOR_DGREY);
    }
    g.setFont(StyleUtils.FONT_NORMAL);
    for (int i = 0; i < getLines().size(); i++) {
      g.drawString((String) lines.elementAt(i), getX() + lp, getY() + tp);
      tp += StyleUtils.FONT_NORMAL.getHeight();
    }
    if (item.clickable) {
      tp = (getPreferredH() - arrow.getHeight()) / 2;
      lp = getWidth() - arrow.getWidth() - getSideGap() - getStyle().getMargin(RIGHT) - 3;
      g.drawImage(arrow, getX() + lp, getY() + tp);
    }
  }

  protected Dimension calcPreferredSize() {
    int height = getStyle().getMargin(TOP) + getStyle().getMargin(BOTTOM)
            + StyleUtils.FONT_BOLD.getHeight() + (getLines().size() * StyleUtils.FONT_NORMAL.getHeight());
    return new Dimension(50, height);
  }

  public Component renderItem(Component cmp, Object item) {
    ItemRenderer renderer = new ItemRenderer();
    renderer.setItem((Item) item);
    return renderer;
  }

  public static class Item {

    final boolean clickable;
    String title;
    String value;

    public Item(String title, String value) {
      this.title = title;
      this.value = value;
      clickable = false;
    }

    public Item(String title, String value, boolean clickable) {
      this.clickable = clickable;
      this.title = title;
      this.value = value;
    }
  }
}
