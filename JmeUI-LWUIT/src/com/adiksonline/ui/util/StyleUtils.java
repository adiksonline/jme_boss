/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui.util;

import com.adiksonline.ui.DisplayManager;
import com.sun.lwuit.Button;
import com.sun.lwuit.Component;
import com.sun.lwuit.Font;
import com.sun.lwuit.TextArea;
import com.sun.lwuit.plaf.Style;
import com.sun.lwuit.plaf.UIManager;
import javax.microedition.lcdui.Display;

/**
 *
 * @author ADIKSONLINE
 */
public class StyleUtils {

  public static final int COLOR_BG;
  public static final int COLOR_FG;
  public static final int COLOR_GREY;
  public static final int COLOR_DGREY;
  public static final int COLOR_SELECT;
  public static final Font FONT_ITALIC = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_ITALIC, Font.SIZE_SMALL);
  public static final Font FONT_BOLD = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_SMALL);
  public static final Font FONT_NORMAL = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_SMALL);
  public static final Font FONT_BBOLD = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_MEDIUM);
  public static final Font FONT_DEFAULT = Font.getDefaultFont();

  static {
    // XXX: Consider using the @link{com.nokia.mid.theme} package instead
    Display d = DisplayManager.getNativeDisplay();
    COLOR_GREY = 0xa0a0a0;
    COLOR_DGREY = 0x505050;
    Style style = UIManager.getInstance().getComponentStyle("Label");
    COLOR_BG = style.getBgColor();
    COLOR_FG = style.getFgColor();
    int temp = d.getColor(Display.COLOR_HIGHLIGHTED_BACKGROUND);
    if (temp == COLOR_BG) {
      temp = COLOR_FG;
    }
    COLOR_SELECT = temp;
  }

  /** Deprecated.
   * Consider using @link{com.nokia.lwuit.components.Hyperlink} or <code>FlatButton</code> style instead.
   * 
   * @param c button to be styled as hyperlink.
   */
  public static void buttonUnderlined(Button c) {
    Style s = c.getStyle();
    s.setBgTransparency(0);
    s = c.getUnselectedStyle();
    s.setBorder(null);
    s.setUnderline(true);
    s.setFont(FONT_NORMAL);
    s.setFgColor(COLOR_SELECT);
    s = c.getPressedStyle();
    s.setBorder(null);
    s.setFgColor(COLOR_SELECT);
    s.setFont(FONT_BOLD);
  }

  public static void multilineLabel(TextArea c) {
    c.setEditable(false);
    c.setGrowByContent(true);
    c.setUIID("Label");
  }

  /**
   * Deprecated.
   * Consider using @link{com.nokia.lwuit.components.ErrorContentTextField} 
   * or <code>ErrorContentHelpText</code> style instead.
   * 
   * @param c component to be styled as the error field.
   */
  public static void errorField(Component c) {
    Style s = c.getStyle();
    s.setFgColor(0xff0000);
    s.setFont(FONT_NORMAL);
    s.setAlignment(Component.CENTER);
    s.setBgTransparency(0);
  }

  public static void normalField(Component c) {
    Style s = c.getStyle();
    s.setFgColor(COLOR_FG);
    s.setFont(FONT_NORMAL);
    s.setBgTransparency(0);
  }

  public static void headerLabel(Component c) {
    Style s = c.getStyle();
    s.setFont(FONT_BOLD);
    s.setFgColor(COLOR_SELECT);
  }

  public static void bigField(Component c) {
    Style s = c.getStyle();
    s.setFgColor(COLOR_FG);
    s.setFont(FONT_DEFAULT);
  }

  public static void footerLabel(Component c) {
    Style s = c.getStyle();
    s.setFgColor(COLOR_GREY);
    s.setFont(FONT_NORMAL);

  }
}
