package com.adiksonline.ui.util;

import com.sun.lwuit.Font;
import java.util.Vector;

/**
 * Utility methods for String handling
 *
 * @author ssaa
 */
public class Strings {

  private final static String ELIPSIS = "...";
  private final static String DELIMITERS = " \n";

  /**
   * Truncates the string to fit the maxWidth. If truncated, an elipsis "..." is displayed to
   * indicate this.
   *
   * @param str
   * @param font
   * @param maxWidth
   * @return String - truncated string with ellipsis added to end of the string
   */
  public static String truncate(final String str, final Font font, int maxWidth) {
    if (str.length() < 10 || font.stringWidth(str) <= maxWidth) {
      return str;
    }

    maxWidth -= font.stringWidth(ELIPSIS);
    StringBuffer truncated = new StringBuffer(str);
    while (truncated.length() > 2 && font.stringWidth(truncated.toString()) > maxWidth) {
      truncated.deleteCharAt(truncated.length() - 1);
    }
    truncated.append(ELIPSIS);

    return truncated.toString();
  }

  private static void splitToLines(Vector vector, String text, final Font font, final int maxWidth, final int maxLines) {
    text = text.trim();
    if (text.length() <= 0) {
      return;
    }
    int lastSpace = 0;
    if (vector.size() == maxLines) {
      String lastLine = (String) vector.lastElement();
      final int eLength = ELIPSIS.length();
      final int sLength = lastLine.length();
      if (!lastLine.endsWith(ELIPSIS) && sLength > eLength + 1) {
        lastLine = lastLine.substring(0, sLength - eLength - 1) + ELIPSIS;
        vector.setElementAt(lastLine, vector.size() - 1);
      }
      return;
    }

    for (int i = 0; i < text.length(); i++) {
      final char character = text.charAt(i);

      if (DELIMITERS.indexOf(character) >= 0) {
        lastSpace = i;
      }
      final int width = font.stringWidth(text.substring(0, i + 1));
      if (width >= maxWidth || character == '\n') {
        if (lastSpace == 0) {
          lastSpace = i > 0 ? i - 1 : i; // Force split very long words into lines
        }
        vector.addElement(text.substring(0, lastSpace).trim());
        splitToLines(vector, text.substring(lastSpace), font, maxWidth, maxLines);
        return;
      }
    }
    vector.addElement(text);
  }

  /**
   * Split a string in to several lines of text which will display within a maximum width.
   *
   * @param str
   * @param font
   * @param maxWidth
   * @return
   */
  public static Vector splitToLines(String text, final Font font, final int maxWidth, int maxLines) {
    Vector result = new Vector();
    splitToLines(result, text, font, maxWidth, maxLines);
    return result;
  }

  public static Vector splitToLines(final String str, final Font font, final int maxWidth) {
    return splitToLines(str, font, maxWidth, -1);
  }
}
