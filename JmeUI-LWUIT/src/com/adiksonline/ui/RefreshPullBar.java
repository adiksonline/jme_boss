/* RefreshPullBar.java
 * 
 * User Interface ME
 * Copyright (c) 2013 eMob Tech (http://www.emobtech.com/)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * 
 * @author ADIKSONLINE
 * 
 * Made a few modifications to work seamlessly hand in, with the Nokia LWUIT port.
 * Also, it doesnt necessarily need to be the first component on the form in this version,
 * in as much as it is the first component in its container regardless of the layout used.
 * Fixed some subtle bugs that arose as well:
 * - it doesnt work if the form is not scrollable
 * - it behaves abnormally when the form gets visible again after it might have been hiden
 * - some other minor issues also fixed.
 */
package com.adiksonline.ui;

import com.adiksonline.ui.util.ImageUtils;
import com.sun.lwuit.Component;
import com.sun.lwuit.Container;
import com.sun.lwuit.Form;
import com.sun.lwuit.Graphics;
import com.sun.lwuit.Image;
import com.sun.lwuit.Label;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.geom.Dimension;
import com.sun.lwuit.layouts.BorderLayout;
import com.sun.lwuit.plaf.Style;
import java.util.Timer;

/**
 * <p> A PullDownRefresh object provides a standard component that can be used to initiate the
 * refreshing of a {@link Form}’s contents. </p> <p> <b>Important:</b> PullDownRefresh must be added
 * ONLY to a {@link Form} and at the FIRST position. </p> <p> It is possible to customize the style,
 * images and labels of this component: </p> <p> <b>Style:</b><br/><br/>To customize the style, do
 * it via Resource Editor, through the UIIDs <b>PullDownRefreshContentPane</b> and
 * <b>PullDownRefreshLabel</b>. </p> <p> <b>Images:</b><br/><br/>To change the images, replace the
 * files <b>"/PullDownRefresh/arrow.png"</b> and <b>"/PullDownRefresh/refresh.png"</b> in the
 * project's resource folder. </p> <p> <b>Labels:</b><br/><br/>To set a new text for the labels, use
 * the methods null {@link PullDownRefresh#setStateDefaultLabel(String)},
 * {@link PullDownRefresh#setStatePullingLabel(String)} and
 * {@link PullDownRefresh#setStateRefreshingLabel(String)}. </p>
 *
 * @author Ernandes Jr. (ernandes@emobtech.com)
 * @version 1.0
 */
public final class RefreshPullBar extends Container implements ActionBar {

  /**
   * <p> Initial Y position. </p>
   */
  private int initialY;
  private boolean initialized;
  private int startY;
  /**
   * <p> State of refreshing. </p>
   */
  private boolean refreshing;
  /**
   * <p> State of triggered. </p>
   */
  private boolean triggered;
  /**
   * <p> State indicator. </p>
   */
  private PullDownRefreshIndicator indicator;
  /**
   * <p> Label. </p>
   */
  private Label label;
  /**
   * <p> Listener. </p>
   */
  private ActionBarListener listener;
  /**
   * <p> Label of default state. </p>
   */
  private String stateDefaulLabel = "Pull down to refresh";
  /**
   * <p> Label of "pulling" state. </p>
   */
  private String statePullingLabel = "Release to refresh";
  /**
   * <p> Label of "refreshing" state. </p>
   */
  private String stateRefreshingLabel = "Refreshing...";
  /**
   * <p> Creates a new instance of PullDownRefresh. </p>
   */
  private static Image arrow = ImageUtils.loadImage("/other/arrow_up.png");
  private static Image refresh = ImageUtils.loadImage("/other/spinner_small.png");

  public RefreshPullBar() {
    setLayout(new BorderLayout());
    //setUIID("PullDownRefreshContentPane");
    getStyle().setBgTransparency(0xff);
    getStyle().setBgColor(0xeeeeee);
    label = new Label(stateDefaulLabel);
    //label.setUIID("PullDownRefreshLabel");
    label.getStyle().setBgTransparency(0);
    addComponent(BorderLayout.CENTER, label);
    indicator = new PullDownRefreshIndicator(arrow, refresh);
    //
    addComponent(BorderLayout.EAST, indicator);
    //
  }

  /**
   * <p> Tells the component that a refresh operation was started programmatically. </p> <p> Call
   * this method when an external event source triggers a programmatic refresh of your {@link Form}.
   * For example, if you use an {@link Timer} object to refresh the contents of the {@link Form}
   * periodically, you would call this method as part of your timer handler. This method updates the
   * state of the refresh component to reflect the in-progress refresh operation. When the refresh
   * operation ends, be sure to call the {@link PullDownRefresh#endRefreshing()} method to return
   * the component to its default state. </p> <p> This method does not trigger any action listener
   * registered. </p>
   */
  public void trigger() {
    if (!refreshing) {
      setHidden(false);
      //
      label.setText(stateRefreshingLabel);
      indicator.setState(PullDownRefreshIndicator.REFRESHING);
      //
      triggered = true;
      refreshing = true;
      if (listener != null) {
        listener.notifyInitialized(this);
      }
    }
  }

  /**
   * <p> Tells the component that a refresh operation has ended. </p> <p> Call this method at the
   * end of any refresh operation (whether it was initiated programmatically or by the user) to
   * return the refresh component to its default state. If the refresh component is at least
   * partially visible, calling this method also hides it. If animations are also enabled, the
   * control is hidden using an animation. </p>
   */
  public void refreshFinished() {
    if (refreshing) {
      setHidden(true);
      //
      label.setText(stateDefaulLabel);
      indicator.setState(PullDownRefreshIndicator.DEFAULT);
      //
      triggered = false;
      refreshing = false;
    }
  }

  /**
   * <p> A Boolean value indicating whether a refresh component has been triggered and is in
   * progress. </p>
   *
   * @return Refreshing (true).
   */
  public boolean isRefreshing() {
    return refreshing;
  }

  /**
   * <p> Sets the default state label. </p>
   *
   * @param defaultLabel Label.
   */
  public void setStateDefaultLabel(String defaultLabel) {
    this.stateDefaulLabel = defaultLabel;
    //
    if (!triggered && !refreshing) {
      label.setText(defaultLabel);
    }
  }

  /**
   * <p> Sets the "pulling" state label. </p>
   *
   * @param pullingLabel Label.
   */
  public void setStatePullingLabel(String pullingLabel) {
    this.statePullingLabel = pullingLabel;
    //
    if (triggered && !refreshing) {
      label.setText(pullingLabel);
    }
  }

  /**
   * <p> Sets the "refreshing" state label. </p>
   *
   * @param refreshingLabel Label.
   */
  public void setStateRefreshingLabel(String refreshingLabel) {
    this.stateRefreshingLabel = refreshingLabel;
    //
    if (refreshing) {
      label.setText(refreshingLabel);
    }
  }

  /**
   * @see com.sun.lwuit.Component#initComponent()
   */
  protected void initComponent() {
    if (!initialized) {
      initialY = getAbsoluteY();
      initialized = true;
    }
    setHidden(true);
    //
    Form formParent = getComponentForm();
    //
    if (formParent != null) {
      formParent.addPointerDraggedListener(new ActionListener() {
        public void actionPerformed(ActionEvent evt) {
          pointerDraggedEvent(evt.getY());
        }
      });
      formParent.addPointerReleasedListener(new ActionListener() {
        public void actionPerformed(ActionEvent evt) {
          pointerReleasedEvent();
        }
      });
    } else {
      throw new IllegalStateException(
              "PullDownRefresh must be added to a Form.");
    }
  }

  /**
   * <p> Called when a pointer dragged event is triggered. </p>
   */
  private void pointerDraggedEvent(int y) {
    if (startY == 0 && y > 0) {
      startY = y;
    }
    if (!refreshing) {
      triggered = isPulling(y);
      //
      if (triggered) {
        indicator.setState(PullDownRefreshIndicator.PULLING);
        label.setText(statePullingLabel);
      } else {
        indicator.setState(PullDownRefreshIndicator.DEFAULT);
        label.setText(stateDefaulLabel);
      }
    }
  }

  /**
   * <p> Called when a pointer released event is triggered. </p>
   */
  private void pointerReleasedEvent() {
    if (!refreshing) {
      if (triggered && startY > 0) {
        trigger();
      } else {
        setHidden(true);
      }
    }
    startY = 0;
  }

  /**
   * <p> Sets the component hidden/visible. </p>
   *
   * @param hidden Hidden (true).
   */
  private void setHidden(boolean hidden) {
    Container parent = getParent();
    int absY = getAbsoluteY();
    //
    if (hidden) {
      if (absY >= (initialY - getHeight()) && absY <= initialY) {
        parent.scrollRectToVisible(
                getX(),
                getHeight(),
                parent.getWidth(),
                parent.getHeight(),
                parent);
      }
    } else {
      if (absY == (initialY - getHeight())) {
        parent.scrollRectToVisible(
                getX(),
                0,
                parent.getWidth(),
                parent.getHeight(),
                parent);
      }
    }
  }

  /**
   * <p> A Boolean value indicating whether a refresh component has been pulled. </p>
   *
   * @return Pulling (true).
   */
  private boolean isPulling(int y) {
    boolean pulling = getAbsoluteY() >= initialY;
    if (startY == 0 || y <= startY) {
      pulling = false;
    }
    return pulling;
  }

  /**
   * @return the listener
   */
  public ActionBarListener getListener() {
    return listener;
  }

  /**
   * @param listener the listener to set
   */
  public void setListener(ActionBarListener listener) {
    this.listener = listener;
  }

  /**
   * <p> Pull down refresh indicator. </p>
   */
  private final class PullDownRefreshIndicator extends Component {

    /**
     * <p> Default state. </p>
     */
    public static final int DEFAULT = 1;
    /**
     * <p> Pulling state. </p>
     */
    public static final int PULLING = 2;
    /**
     * <p> Refreshing state. </p>
     */
    public static final int REFRESHING = 3;
    /**
     * <p> State. </p>
     */
    private int state;
    /**
     * <p> Sprite. </p>
     */
    private Image[] sprite;
    /**
     * <p> Refresh sprite. </p>
     */
    private Image[] refreshSprite;
    /**
     * <p> Arrow forward sprite. </p>
     */
    private Image[] arrowForwardSprite;
    /**
     * <p> Arrow backward sprite. </p>
     */
    private Image[] arrowBackwardSprite;
    /**
     * <p> In loop. </p>
     */
    private boolean loop;
    /**
     * <p> Cursor. </p>
     */
    private int cursor;
    /**
     * <p> Last update. </p>
     */
    private long lastUpdate;

    /**
     * <p> Creates a new instance of PullDownRefreshIndicator. </p>
     *
     * @param arrowImage Arrow image.
     * @param refreshImage Refresh image.
     */
    public PullDownRefreshIndicator(Image arrowImage, Image refreshImage) {
      setUIID("PullDownRefreshIndicator");
      //
      Image refresh45 = refreshImage.rotate(45);
      //
      refreshSprite = new Image[]{
        refreshImage,
        refresh45,
        refreshImage.rotate(90),
        refresh45.rotate(90),
        refreshImage.rotate(180),
        refresh45.rotate(180),
        refreshImage.rotate(270),
        refresh45.rotate(270)};
      arrowForwardSprite = new Image[]{
        arrowImage,
        arrowImage.rotate(45),
        arrowImage.rotate(90),
        arrowImage.rotate(135),
        arrowImage.rotate(180)};
      arrowBackwardSprite = new Image[]{
        arrowForwardSprite[4],
        arrowForwardSprite[3],
        arrowForwardSprite[2],
        arrowForwardSprite[1],
        arrowForwardSprite[0]};
      //
      setState(DEFAULT);
    }

    /**
     * <p> Sets the state. </p>
     *
     * @param state State.
     * @see PullDownRefreshIndicator#DEFAULT
     * @see PullDownRefreshIndicator#PULLING
     * @see PullDownRefreshIndicator#REFRESHING
     */
    public void setState(int state) {
      if (this.state != state) {
        loop = false;
        cursor = 0;
        this.state = state;
        //
        if (state == DEFAULT) {
          sprite = arrowForwardSprite;
        } else if (state == PULLING) {
          sprite = arrowBackwardSprite;
        } else {
          sprite = refreshSprite;
          loop = true;
        }
        //
        if (getComponentForm() != null) {
          getComponentForm().registerAnimated(this);
        }
      }
    }

    /**
     * @see com.sun.lwuit.Component#animate()
     */
    public boolean animate() {
      final long INTERVAL = 50;
      //
      boolean animate = false;
      long now = System.currentTimeMillis();
      //
      if ((now - lastUpdate) > INTERVAL) {
        cursor++;
        animate = true;
        lastUpdate = now;
      }
      //
      return animate;
    }

    /**
     * @see com.sun.lwuit.Component#paint(com.sun.lwuit.Graphics)
     */
    public void paint(Graphics g) {
      int index = Math.abs(cursor % sprite.length);
      Image img = sprite[index];
      //
      g.drawImage(
              img,
              getX() + (getWidth() - img.getWidth()) / 2,
              getY() + (getHeight() - img.getHeight()) / 2);
      //
      if (!loop && index + 1 == sprite.length) { //stop last frame?
        Form form = getComponentForm();
        if (form != null) {
          getComponentForm().deregisterAnimated(this);
        }
      }
    }

    /**
     * @see com.sun.lwuit.Component#initComponent()
     */
    protected void initComponent() {
      getStyle().setBgTransparency(0);
      getSelectedStyle().setBgTransparency(0);
      getUnselectedStyle().setBgTransparency(0);
      //
      getComponentForm().registerAnimated(this);
    }

    /**
     * @see com.sun.lwuit.Component#calcPreferredSize()
     */
    protected Dimension calcPreferredSize() {
      Style style = getStyle();
      //
      return new Dimension(
              refreshSprite[0].getWidth()
              + style.getPadding(LEFT)
              + style.getPadding(RIGHT),
              refreshSprite[0].getHeight()
              + style.getPadding(TOP)
              + style.getPadding(BOTTOM));
    }
  }
}
