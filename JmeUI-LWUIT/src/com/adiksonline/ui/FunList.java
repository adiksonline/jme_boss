/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui;

import com.adiksonline.core.util.UpdateListener;
import com.adiksonline.core.util.ItemModel;
import com.sun.lwuit.Component;
import com.sun.lwuit.Container;
import com.sun.lwuit.Display;
import com.sun.lwuit.Graphics;
import com.sun.lwuit.Label;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.events.SelectionListener;
import com.sun.lwuit.geom.Dimension;
import com.sun.lwuit.layouts.BoxLayout;
import com.sun.lwuit.plaf.Style;
import com.sun.lwuit.plaf.UIManager;
import com.sun.lwuit.util.EventDispatcher;

/**
 *
 * @author ADIKSONLINE
 */
public class FunList extends Container implements UpdateListener {

  private EventDispatcher actionDispatcher = new EventDispatcher();
  private EventDispatcher selectionDispatcher = new EventDispatcher();
  private FunListRenderer renderer = new DefaultRenderer();
  private ItemModel model;
  private String cellUIID = null;
  private int sIndex = -1;

  public FunList() {
    this(new ItemModel());
  }

  private FunList(ItemModel model) {
    setLayout(new BoxLayout(BoxLayout.Y_AXIS));
    setModelInternal(model);
  }

  private void setModelInternal(ItemModel model) {
    if (this.model != model) {
      if (this.model != null) {
        this.model.removeListener(this);
      }
      this.model = (model == null) ? new ItemModel() : model;
      this.model.addListener(this);
    }
  }

  public void setModel(ItemModel model) {
    setModelInternal(model);
  }

  public ItemModel getModel() {
    return model;
  }

  public synchronized void addItem(Object item) {
    model.addItem(item);
  }

  public synchronized void addItemAt(int index, Object item) {
    model.insertAt(index, item);
  }

  public synchronized Object getItemAt(int index) {
    return model.getItem(index);
  }

  public synchronized int getSelectedIndex() {
    return sIndex;
  }

  public synchronized Object getSelectedItem() {
    Object item = null;
    try {
      item = model.getItem(sIndex);
    } catch (ArrayIndexOutOfBoundsException ex) {
    }
    return item;
  }

  public synchronized void setSelectedIndex(int index) {
    if (index < 0 || index >= getComponentCount()) {
      return;
    }
    ((FunListItem) getComponentAt(index)).setSelected(true);
    selectionDispatcher.fireSelectionEvent(sIndex, index);
    sIndex = index;
  }

  public synchronized void setItem(int index, Object item) {
    model.setItem(index, item);
  }

  public synchronized int getSize() {
    return model.getSize();
  }

  public synchronized void removeItemAt(int index) {
    model.removeAt(index);
  }

  public synchronized void removeAll() {
    model.removeAll();
  }

  public synchronized void scrollTo(int index) {
    if (index >= 0 && getComponentCount() - index > 0) {
      getComponentAt(index).requestFocus();
      repaint();
    }
  }

  public synchronized void setCellUIID(String uiid) {
    if (!model.isEmpty()) {
      throw new IllegalStateException("Item UIID cannot be set. List not empty");
    }
    cellUIID = uiid;
  }

  public synchronized String getCellUIID() {
    return cellUIID;
  }

  public void addActionListener(ActionListener listener) {
    actionDispatcher.addListener(listener);
  }

  public void removeActionListener(ActionListener listener) {
    actionDispatcher.removeListener(listener);
  }

  public void addSelectionListener(SelectionListener listener) {
    selectionDispatcher.addListener(listener);
  }

  public void removeSelectionListener(SelectionListener listener) {
    selectionDispatcher.removeListener(listener);
  }

  public void setRenderer(FunListRenderer renderer) {
    if (renderer == null) {
      throw new NullPointerException("List renderer cannot be null");
    }
    this.renderer = renderer;
  }

  public void dataRemoved(int index, Object data) {
    removeComponent(getComponentAt(index));
  }

  public void dataAdded(int index, Object data) {
    addComponent(index, new FunListItem(data));
  }

  public void dataChanged(int index, Object data) {
    ((FunListItem) getComponentAt(index)).setItem(data);
  }

  public void dataCleared() {
    super.removeAll();
  }

  private class FunListItem extends Component {

    private boolean selected = false;
    private int dragCounter;
    private final int MAX_THRESHOLD = 3;
    private Component renderer;

    public FunListItem(Object item) {
      setItemInternal(item);
      if (cellUIID != null) {
        setUIID(cellUIID);
      } else {
        //all these should be done from inside the res file
        Style s = getUnselectedStyle();
        s.setPadding(10, 10, 10, 10);
        s.setMargin(0, 0, 0, 0);
        s = getSelectedStyle();
        s.setPadding(10, 10, 10, 10);
        s.setMargin(0, 0, 0, 0);
        //s.setBgColor(0xe1e1e1);
        setPressedStyle(s);
      }
    }

    private void setItemInternal(Object item) {
      renderer = FunList.this.renderer.renderItem(this, item);
      renderer.setWidth(Display.getInstance().getDisplayWidth());
    }

    public void setItem(Object item) {
      setItemInternal(item);
    }

    public void pointerPressed(int x, int y) {
      setSelectedIndex(getComponentIndex(this));
      dragCounter = 0;
      super.pointerPressed(x, y);
    }

    public void setSelected(boolean selected) {
      this.selected = selected;
      repaint();
    }

    public void pointerReleased(int x, int y) {
      super.pointerReleased(x, y);
      if (selected) {
        actionDispatcher.fireActionEvent(new ActionEvent(FunList.this, x, y));
      }
      selected = false;
      repaint();
    }

    public void pointerDragged(int x, int y) {
      dragCounter++;
      if (selected && dragCounter > MAX_THRESHOLD) {
        selected = false;
        repaint();
      }
      super.pointerDragged(x, y);
    }

    public void paint(Graphics g) {
      super.paint(g);
      UIManager.getInstance().getLookAndFeel().setFG(g, this);
      renderer.setX(getX());
      renderer.setY(getY());
      renderer.paint(g);
      if (cellUIID == null) {
        g.setColor(0xd0d0d0);
        g.drawLine(getX() + 2, getY() + getPreferredH() - 1, getX() + getWidth() - 2, getY() + getPreferredH() - 1);
      }
    }

    protected Dimension calcPreferredSize() {
      return renderer.getPreferredSize();
    }
  }

  private class DefaultRenderer implements FunListRenderer {

    public Component renderItem(Component cmp, Object item) {
      return new Label(item.toString());
    }
  }
}
