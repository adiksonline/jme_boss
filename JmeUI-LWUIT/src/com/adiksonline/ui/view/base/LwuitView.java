/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui.view.base;

import com.sun.lwuit.Command;
import com.sun.lwuit.LwuitForm;
import com.sun.lwuit.events.ActionListener;

/**
 *
 * @author ADIKSONLINE
 */
public abstract class LwuitView extends LwuitForm implements Viewable, ActionListener {

  private final Command backCommand = new Command("Back");

  public LwuitView() {
    this("Lwuit View");
  }

  public LwuitView(String title) {
    super(title);
    setBackCommand(backCommand);
    addCommandListener(this);
  }

  /**
   *
   * @param forward the value of forward
   */
  public void activate(boolean forward) {
    show();
  }

  /**
   *
   * @param forward the value of forward
   */
  public void deactivate(boolean forward) {
  }

  /**
   * @return the backCommand
   */
  public Command getBackCommand() {
    return backCommand;
  }
}
