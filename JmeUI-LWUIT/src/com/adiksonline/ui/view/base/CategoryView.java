/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui.view.base;

import com.nokia.lwuit.CategoryBarProvider;

/**
 *
 * @author ADIKSONLINE
 */
public abstract class CategoryView extends LwuitView implements CategoryBarProvider.ElementListener {

  private CategoryBarProvider catbar;

  public CategoryView() {
    this("Category view");
  }

  public CategoryView(String title) {
    super(title);
  }

  /**
   *
   * @param forward the value of forward
   */
  public void activate(boolean forward) {
    Viewable view = getSelectedView();
    // avoids unneccessary recursion
    if (this == view) {
      show();
    } else {
      view.activate(forward);
    }
    CategoryBarProvider c = getCatBar();
    c.setVisibility(true);
  }

  private CategoryBarProvider getCatBar() {
    if (catbar == null) {
      catbar = getCategoryBar();
      if (catbar == null) {
        throw new NullPointerException("Category bar cannot be null");
      }
      catbar.setElementListener(this);
    }
    return catbar;
  }

  /**
   *
   * @param forward the value of forward
   */
  public void deactivate(boolean forward) {
    getCatBar().setVisibility(false);
  }

  protected void revalidateCategoryBar() {
    CategoryBarProvider categoryBar = getCategoryBar();
    if (categoryBar == null) {
      throw new NullPointerException("Category bar cannot be null");
    }
    catbar.setElementListener(null);
    catbar = categoryBar;
    catbar.setElementListener(this);
  }

  protected abstract CategoryBarProvider getCategoryBar();

  protected abstract Viewable getSelectedView();
}
