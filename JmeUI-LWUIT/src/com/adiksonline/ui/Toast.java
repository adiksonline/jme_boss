/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui;

import com.nokia.lwuit.components.NotificationBar;
import com.sun.lwuit.Image;

/**
 *
 * @author ADIKSONLINE
 */
public class Toast {

  public static void toast(String text) {
    toast(null, text);
  }

  public static void toast(Image image, String text) {
    NotificationBar nb = new NotificationBar(image, text);
    nb.showNoitification();
  }

  public static void toast(Image image, String title, String text) {
    NotificationBar nb = new NotificationBar(image, title, text);
    nb.showNoitification();
  }
}
