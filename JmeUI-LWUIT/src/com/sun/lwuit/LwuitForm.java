/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.lwuit;

import com.adiksonline.ui.DisplayManager;
import com.adiksonline.ui.Screenable;
import com.sun.lwuit.animations.CommonTransitions;

/**
 *
 * @author ADIKSONLINE
 */
public class LwuitForm extends Form implements Screenable {

  public LwuitForm() {
    this("LWUIT Form");
  }

  public LwuitForm(String title) {
    super(title);
    setTransitionOutAnimator(CommonTransitions.createFade(350));
    getUnselectedStyle().setBgTransparency(255);
  }

  public void addComponentToForm(Object constraints, Component cmp) {
    super.addComponentToForm(constraints, cmp);
  }

  public void show() {
    DisplayManager.setCurrent(this);
    super.show();
  }

  public void showBack() {
    DisplayManager.setCurrent(this);
    super.showBack();
  }
}
