/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui.view.base;

import com.adiksonline.ui.Screenable;

/**
 *
 * @author ADIKSONLINE
 */
public interface Viewable extends Screenable {

  /**
   *
   * @param forward the direction of flow into view
   */
  public void activate(boolean forward);

  /**
   *
   * @param forward the direction of flow out of view
   */
  public void deactivate(boolean forward);
}
