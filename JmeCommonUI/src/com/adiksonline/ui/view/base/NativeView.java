/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui.view.base;

import com.adiksonline.ui.NativeForm;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;

/**
 *
 * @author ADIKSONLINE
 */
public abstract class NativeView extends NativeForm implements Viewable, CommandListener {
    private final Command backCommand = new Command("Back", Command.BACK, 0);
    
    public NativeView(){
        this("Native view");
    }
    
    public NativeView(String title){
        super(title);
        addCommand(backCommand);
        setCommandListener(this);
    }

  /**
   *
   * @param forward the value of forward
   */
  public void activate(boolean forward) {
    show();
  }

  /**
   *
   * @param forward the value of forward
   */
  public void deactivate(boolean forward) {
  }

    /**
     * @return the backCommand
     */
    public Command getBackCommand() {
        return backCommand;
    }
}
