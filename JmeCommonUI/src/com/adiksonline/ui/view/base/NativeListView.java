/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui.view.base;

import com.adiksonline.ui.NativeList;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;

/**
 *
 * @author ADIKSONLINE
 */
public abstract class NativeListView extends NativeList implements Viewable, CommandListener {
    private final Command backCommand = new Command("Back", Command.BACK, 0);
    
    public NativeListView(){
        this("Native list", IMPLICIT);
    }
    
    public NativeListView(String title){
        this(title, NativeList.IMPLICIT);
    }
    
    public NativeListView(String title, int listType){
        super(title, listType);
        addCommand(backCommand);
        setCommandListener(this);
    }

    /**
     * @return the backCommand
     */
    public Command getBackCommand() {
        return backCommand;
    }
}
