/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui;

/**
 *
 * @author ADIKSONLINE
 */
public interface ActionBar {

  public void trigger();

  public void refreshFinished();

  public static interface ActionBarListener {

    public void notifyInitialized(ActionBar actionBar);
  }
}
