/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui;

import javax.microedition.lcdui.List;

/**
 *
 * @author ADIKSONLINE
 */
public class NativeList extends List implements Screenable {

  public NativeList() {
    this("Native list", IMPLICIT);
  }

  public NativeList(String title, int listType) {
    super(title, listType);
  }

  public void show() {
    AbsDisplayManager.setCurrent(this);
  }

  public void showBack() {
    show();
  }
}
