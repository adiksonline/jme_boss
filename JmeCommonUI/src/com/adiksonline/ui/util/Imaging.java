/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui.util;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Date;
import javax.microedition.io.Connector;
import javax.microedition.io.file.FileConnection;

/**
 *
 * @author Oladeji
 */
public class Imaging {

  private static final int ALPHA = 0xFF000000;
  private static final int RED = 0x00FF0000;
  private static final int GREEN = 0x0000FF00;
  private static final int BLUE = 0x000000FF;
  private static int[] NLT = null;
  //negative lookup table. This follows
  //the LAZY initialization pattern which means that this table will never be
  //precomputed, thereby saving client code that never converts an image to
  //negative the overhead of computing the table. The first call to
  //ImageUtils#toNegative computes the table and subsequent calls perform
  //faster because they do not have to compute the table anymore.

  public static boolean saveImage(byte[] imageData) {
    long secs = new Date().getTime() / 1000;
    String fname = "BP_" + secs + ".jpg";
    return saveImage(imageData, fname);
  }

  public static boolean saveImage(byte[] imageData, String fname) {
    FileConnection fileCon = null;
    DataOutputStream os = null;
    try {
      fileCon = (FileConnection) Connector.open(
              System.getProperty("fileconn.dir.photos") + fname, Connector.READ_WRITE);
      if (!fileCon.exists()) {
        fileCon.create();
      }
      os = fileCon.openDataOutputStream();
      os.write(imageData, 0, imageData.length);
      return true;
    } catch (Exception ex) {
      ex.printStackTrace();
      return false;
    } finally {
      try {
        os.close();
        fileCon.close();
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    }
  }

  /**
   * Converts an image to grayscale. Alpha values are left constant.
   *
   * @param input ARGB values of the input image packed in single ints
   * @return ARGB values of the grayscale image packed; one pixel per int
   */
  public static int[] toGrayScale(final int[] input) {
    //take care of bad vales, trust nobody :)
    if (input == null) {
      return null;
    }

    int[] output = new int[input.length];
    int a = 0, r = 0, g = 0, b = 0;

    for (int i = 0; i < input.length; i++) {
      b = input[i] & BLUE;
      g = (input[i] & GREEN) >> 8;
      r = (input[i] & RED) >> 16;

      //special beast. Don't bit shift so as to bypass the overhead
      //of shifting right and back to left redundantly
      a = input[i] & ALPHA;

      int gp = (r + g + b) / 3;
      int dest = a | (gp << 16) | (gp << 8) | gp;
      output[i] = dest;
    }
    return output;
  }

  /**
   * Washes an image with a color. The result is a monochrome image in the specified color. Washing
   * with black (
   * <code>0,0,0</code>) results in a <b>dark</b> grayscale image. Washing with white (
   * <code>255,255,255</code>) results in a <b>bright</b> grayscale image. Please do not use this
   * method for converting to grayscale, there's a specialized method for that.
   *
   * @param input ARGB values of the source image.
   * @param r the red component of the destination color.
   * @param g the green component of the destination color.
   * @param b the blue component of the destination color.
   * @return a monochrome copy of the source image in the destination color.
   * @see ImageUtils#toGrayScale(int[])
   */
  public static int[] washWithColor(final int[] input, final int r,
          final int g, final int b) {

    if (input == null) {
      return null;
    }

    int[] output = new int[input.length];
    int a, dr, dg, db;

    for (int i = 0; i < input.length; i++) {
      db = input[i] & BLUE;
      dg = (input[i] & GREEN) >> 8;
      dr = (input[i] & RED) >> 16;

      //special beast. Don't bit shift so as to bypass the overhead
      //of shifting right and back to left redundantly
      a = input[i] & ALPHA;

      //DEEP MAGIC
      int gp = (dr + dg + db) / 3;
      int rr = (dr + gp + r) / 3;
      int gg = (dg + gp + g) / 3;
      int bb = (db + gp + b) / 3;
      int dest = a | (rr << 16) | (gg << 8) | (bb);
      //END OF DEEP MAGIC
      output[i] = dest;
    }
    return output;
  }

  /**
   * @see ImageUtils#toColor(int[], int, int, int)
   * @see ImageUtils#splitToChannels(int)
   * @param input
   * @param color
   * @return
   */
  public static int[] washWithColor(final int[] input, final int color) {
    int[] argb = splitToChannels(color);
    return washWithColor(input, argb[1], argb[2], argb[3]);
  }

  /**
   * Washes an image with a color. The result is a monochrome image in the specified color. Washing
   * with black (
   * <code>0,0,0</code>) results in a <b>dark</b> grayscale image. Washing with white (
   * <code>255,255,255</code>) results in a <b>bright</b> grayscale image. Please do not use this
   * method for converting to grayscale, there's a specialized method for that.
   *
   * @param input ARGB values of the source image.
   * @param r the red component of the destination color.
   * @param g the green component of the destination color.
   * @param b the blue component of the destination color.
   * @return a monochrome copy of the source image in the destination color.
   * @see ImageUtils#toGrayScale(int[])
   */
  public static int[] toColor(final int[] input, final int r,
          final int g, final int b) {

    if (input == null) {
      return null;
    }

    int[] output = new int[input.length];
    int a, dr, dg, db;

    for (int i = 0; i < input.length; i++) {
      db = input[i] & BLUE;
      dg = (input[i] & GREEN) >> 8;
      dr = (input[i] & RED) >> 16;

      //special beast. Don't bit shift so as to bypass the overhead
      //of shifting right and back to left redundantly
      a = input[i] & ALPHA;

      //DEEP MAGIC
      int gp = (dr + dg + db) / 3;

      int rr = (gp + r) >> 1;
      int gg = (gp + g) >> 1;
      int bb = (gp + b) >> 1;
      int dest = a | (rr << 16) | (gg << 8) | (bb);
      //END OF DEEP MAGIC
      output[i] = dest;
    }
    return output;
  }

  /**
   * @see ImageUtils#toColor(int[], int, int, int)
   * @see ImageUtils#splitToChannels(int)
   * @param input
   * @param color
   * @return
   */
  public static int[] toColor(final int[] input, final int color) {
    int[] argb = splitToChannels(color);
    return toColor(input, argb[1], argb[2], argb[3]);
  }

  /**
   * Converts an image to negative.
   *
   * @param input ARGB values of source image packed as single ints.
   * @return negative image.
   */
  public static int[] toNegative(final int[] input) {
    if (input == null) {
      return null;
    }

    if (NLT == null) {
      computeNegativeLookUpTable();
    }

    int[] output = new int[input.length];
    int a = 0, r = 0, g = 0, b = 0;

    for (int i = 0; i < input.length; i++) {
      b = input[i] & BLUE;
      g = (input[i] & GREEN) >> 8;
      r = (input[i] & RED) >> 16;

      //special beast. Don't bit shift so as to bypass the overhead
      //of shifting right and back to left redundantly
      a = input[i] & ALPHA;

      int dest = a | (NLT[r] << 16) | (NLT[g] << 8) | NLT[b];
      output[i] = dest;
    }
    return output;
  }

  /**
   * Helper method for computing the negative look-up table.
   */
  private static void computeNegativeLookUpTable() {
    NLT = new int[256];
    for (int i = 0; i < 256; i++) {
      NLT[i] = 255 - i;
    }
  }

  /**
   * Flips an image horizontally.
   *
   * @param input ARGB values of the source image.
   * @param w width of source image.
   * @param h height of source image.
   * @return flipped image.
   */
  public static int[] flipHorizontal(final int[] input, final int w,
          final int h) {

    //take care of bad data, trust nobody :)
    if (input == null || input.length != w * h) {
      return null;
    }

    int[] output = new int[input.length];
    int aw = w - 1;

    for (int y = 0; y < h; y++) {
      for (int x = 0; x < w; x++) {
        output[toLinear(x, y, w, h)] = input[toLinear(aw - x, y, w, h)];
      }
    }
    return output;
  }

  /**
   * Flips an image vertically.
   *
   * @param input ARGB values of the source image.
   * @param w width of source image.
   * @param h height of source image.
   * @return flipped image.
   */
  public static int[] flipVertical(final int[] input, final int w,
          final int h) {

    //take care of bad data, trust nobody :)
    if (input == null || input.length != w * h) {
      return null;
    }

    int[] output = new int[input.length];
    int ah = h - 1;

    for (int y = 0; y < h; y++) {
      for (int x = 0; x < w; x++) {
        output[toLinear(x, y, w, h)] = input[toLinear(x, ah - y, w, h)];
      }
    }
    return output;
  }

  /**
   * Converts a linear position to a point in a grid.
   *
   * @param p position
   * @param w width of grid
   * @param h height of grid
   * @return a 2-element array {y, x}. Points start at 0,0
   */
  private static int[] toPoint(final int p, final int w, final int h) {
    if (p == 0) {
      return new int[]{0, 0};
    } else if (p < 0 || p >= w * h) //this automagically takes care of w<=0 or h<=0 bad data!
    {
      return null;
    } else {
      return new int[]{p / w, p % w};
    }
  }

  /**
   * Converts a point in a grid to a linear position.
   *
   * @param x the horizontal component of the grid position
   * @param y the vertical component of the grid position
   * @param w the width of the grid
   * @param h the height of the grid
   * @return the linear position of the point. Positions start at 0.
   */
  private static int toLinear(final int x, final int y, final int w, final int h) {
    if (x < 0 || y < 0) {
      return Integer.MIN_VALUE;
    } else {
      return y * w + x;
    }
  }

  /**
   * Splits a packed color value (in ARGB) into individual channels
   *
   * @param argb
   * @return
   */
  public static int[] splitToChannels(int argb) {
    /*if (argb < 0)
     argb = 0;
     else if (argb > 0xffffffff)
     argb = 0xffffffff;*/

    int a = (argb & ALPHA) >> 24;
    int r = (argb & RED) >> 16;
    int g = (argb & GREEN) >> 8;
    int b = argb & BLUE;
    return new int[]{a, r, g, b};
  }
}