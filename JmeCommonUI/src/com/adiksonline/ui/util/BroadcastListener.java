/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui.util;

/**
 *
 * @author ADIKSONLINE
 */
public interface BroadcastListener {

  public void itemPosted(Class cls);

  public void itemUpdated(Object item, Class cls);
  
}
