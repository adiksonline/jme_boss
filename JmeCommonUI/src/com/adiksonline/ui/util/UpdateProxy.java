/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui.util;

import java.util.Vector;

/**
 *
 * @author ADIKSONLINE
 */
public class UpdateProxy {

  private static final Vector broadcastListeners = new Vector();

  public synchronized static void invalidateListeners() {
    broadcastListeners.removeAllElements();
  }

  public static void addListener(BroadcastListener listener) {
    if (!broadcastListeners.contains(listener)) {
      broadcastListeners.addElement(listener);
    }
  }

  public static void removeListener(BroadcastListener listener) {
    broadcastListeners.removeElement(listener);
  }

  public synchronized static void broadcastUpdate(final ItemBroadcast broadcast) {
    for (int i = 0; i < broadcastListeners.size(); i++) {
      BroadcastListener cul = (BroadcastListener) broadcastListeners.elementAt(i);
      switch (broadcast.type) {
        case ItemBroadcast.POSTED:
          cul.itemPosted(broadcast.cls);
          break;
        case ItemBroadcast.UPDATED:
          cul.itemUpdated(broadcast.item, broadcast.cls);
          break;
      }
    }
  }

  public static class ItemBroadcast {

    public static final int POSTED = 0;
    public static final int UPDATED = 1;
    private int type;
    private Object item;
    private Class cls;

    public ItemBroadcast(final int type, final Class cls) {
      this.type = type;
      this.cls = cls;
    }

    public ItemBroadcast(final int type, final Object item, final Class cls) {
      this.type = type;
      this.item = item;
      this.cls = cls;
    }
  }
}
