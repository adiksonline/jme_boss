/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui;

import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Gauge;

/**
 *
 * @author ADIKSONLINE
 */
public class Dialogs {

    private static final AbsNativeDialog dialog = new AbsNativeDialog(null){};
    private static final int TIMEOUT = 3000;
    private static final Command hideCommand = new Command("Hide", Command.BACK, 0);

    public static void message(String title, String message) {
      message(title, message, false);
    }
    
    public static void message(String title, String message, boolean isBusy) {
        dialog.setTitle(title);
        dialog.setString(message);
        dialog.setIndicator(isBusy ? new Gauge(null, false, Gauge.INDEFINITE, Gauge.CONTINUOUS_RUNNING) : null);
        dialog.setType(AlertType.INFO);
        dialog.setTimeout(isBusy ? AbsNativeDialog.FOREVER : TIMEOUT);
        if (isBusy) {
            dialog.addCommand(hideCommand);
        } else {
            dialog.removeCommand(hideCommand);
        }
        dialog.show(AbsDisplayManager.getLastForm());
    }

    public static void error(String title, String message) {
        dialog.setTitle(title);
        dialog.setString(message);
        dialog.setIndicator(null);
        dialog.setTimeout(TIMEOUT);
        dialog.setType(AlertType.ERROR);
        dialog.removeCommand(hideCommand);
        dialog.show(AbsDisplayManager.getLastForm());
    }

    public static void hideDialog() {
        dialog.dispose();
    }
}