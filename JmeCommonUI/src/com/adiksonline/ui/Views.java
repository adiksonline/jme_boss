/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui;

import com.adiksonline.ui.view.base.Viewable;
import java.util.Stack;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Gauge;

/**
 *
 * @author ADIKSONLINE
 */
public class Views {

  private static ExitListener listener;
  private static final Stack views = new Stack();

  public synchronized static void push(Viewable view) {
    if (!views.empty()) {
      ((Viewable) views.peek()).deactivate(true);
    }
    views.push(view);
    view.activate(true);
  }

  public synchronized static void pop() {
    if (views.size() <= 1) {
      exitApp();
    } else {
      ((Viewable) views.pop()).deactivate(false);
      ((Viewable) views.peek()).activate(false);
    }
  }

  public synchronized static void revalidate() {
    if (!views.empty()) {
      ((Viewable) views.peek()).activate(true);
    }
  }

  public synchronized static void invalidate() {
    while (views.size() > 1) {
      ((Viewable)views.pop()).deactivate(false);
    }
    revalidate();
  }
  
  public synchronized static void reset() {
    while (!views.empty()) {
      ((Viewable)views.pop()).deactivate(false);
    }
  }

  public synchronized static Viewable currentView() {
    return views.empty() ? null : (Viewable) views.peek();
  }

  public static void registerExitListener(ExitListener listener) {
    Views.listener = listener;
  }

  private static void exitApp() {
    String appName = AbsDisplayManager.midlet.getAppProperty("MIDlet-Name");
    final AbsNativeDialog a = new AbsNativeDialog(appName, "Do you want to close this app?", null, null){};
    final Command yesCommand = new Command("Yes", Command.OK, 0);
    final Command noCommand = new Command("No", Command.BACK, 0);
    a.addCommand(yesCommand);
    a.addCommand(noCommand);
    a.setCommandListener(new CommandListener() {
      public void commandAction(Command c, Displayable d) {
        if (c == yesCommand) {
          a.removeCommand(yesCommand);
          a.setString("Please wait...");
          a.setIndicator(new Gauge(null, false, Gauge.INDEFINITE, Gauge.CONTINUOUS_RUNNING));
          a.setCommandListener(new CommandListener() {
            public void commandAction(Command c, Displayable d) {
            }
          });
          if (listener != null) {
            listener.exitApp();
          }
        } else if (c == noCommand) {
          a.dispose();
        }
      }
    });
    a.setTimeout(AbsNativeDialog.FOREVER);
    a.show();
  }
}
