/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Image;

/**
 *
 * @author ADIKSONLINE
 */
abstract class AbsNativeDialog extends Alert implements CommandListener {

  Displayable next;

  public AbsNativeDialog(String title) {
    super(title);
    setTimeout(FOREVER);
    setCommandListener(this);
  }

  public AbsNativeDialog(String title, String alertText, Image alertImage, AlertType alertType) {
    super(title, alertText, alertImage, alertType);
    setCommandListener(this);
  }

  public AbsNativeDialog(String title, String alertText, AlertType alertType) {
    this(title, alertText, null, alertType);
  }

  public void show() {
    AbsDisplayManager.setCurrent(this);
  }

  public void show(Displayable next) {
    AbsDisplayManager.setCurrent(this, next);
  }

  public void commandAction(Command c, Displayable d) {
    if (c == DISMISS_COMMAND) {
      showNext();
    }
  }

  public void showNext() {
    if (next != null) {
      AbsDisplayManager.setCurrent(next);
      next = null;
    }
  }

  public Displayable getNext() {
    return next;
  }

  /**
   * Proxy method to dispose the dialog and show the next displayable
   */
  public void dispose() {
    showNext();
  }
}
