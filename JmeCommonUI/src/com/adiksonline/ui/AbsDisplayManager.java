/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.midlet.MIDlet;

/**
 *
 * @author ADIKSONLINE
 */
abstract class AbsDisplayManager {

  private static Displayable returnTo;
  private static Displayable current;
  static Display nativeDisplay;
  static MIDlet midlet;

  public static void init(MIDlet m) {
    current = (nativeDisplay = Display.getDisplay(m)).getCurrent();
    midlet = m;
  }

  public static Displayable getVisible() {
    return nativeDisplay.getCurrent();
  }

  public static void setCurrent(Displayable d) {
    if (d instanceof javax.microedition.lcdui.Alert) {
      if (!(d instanceof AbsNativeDialog)) {
        throw new IllegalArgumentException("An instance of \"NativeDialog\" is expected instead");
      }
      if (((AbsNativeDialog) d).next == null) {
        ((AbsNativeDialog) d).next = returnTo;
      }
    } else {
      returnTo = d;
    }
    current = d;
    if (current instanceof javax.microedition.lcdui.Alert && nativeDisplay.getCurrent() instanceof javax.microedition.lcdui.Alert) {
      nativeDisplay.setCurrent(new BlackCanvas());
    } else {
      nativeDisplay.setCurrent(current);
    }
  }

  public static void setCurrent(AbsNativeDialog a, Displayable d) {
    a.next = d;
    setCurrent(a);
  }

  public static Displayable getCurrent() {
    return current;
  }
    
    static Displayable getLastForm() {
        return returnTo;
    }

  /**
   * @return the nativeDisplay
   */
  public static Display getNativeDisplay() {
    if (nativeDisplay == null) {
      throw new IllegalStateException("DisplayManager has not been initialized. Call DisplayManager.init(MIDlet m);");
    }
    return nativeDisplay;
  }

  private static class BlackCanvas extends Canvas {

    protected void paint(Graphics g) {
      g.setColor(0);
      g.fillRect(0, 0, getWidth(), getHeight());
      AbsDisplayManager.setCurrent(AbsDisplayManager.getCurrent());
    }
  }
}
