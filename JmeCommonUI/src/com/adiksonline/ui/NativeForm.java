/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.ui;

import javax.microedition.lcdui.Form;

/**
 *
 * @author ADIKSONLINE
 */
public class NativeForm extends Form implements Screenable {

  public NativeForm() {
    this("Native form");
  }

  public NativeForm(String title) {
    super(title);
  }

  public void show() {
    AbsDisplayManager.setCurrent(this);
  }

  public void showBack() {
    show();
  }
}
