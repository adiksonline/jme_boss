/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.core.parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;

/**
 *
 * @author ADIKSONLINE
 */
public abstract class PseudoXmlParser {

  protected final StringBuffer startBuffer = new StringBuffer();
  protected final StringBuffer endBuffer = new StringBuffer();
  protected final StringBuffer keyBuffer = new StringBuffer();
  protected final StringBuffer valueBuffer = new StringBuffer();
  protected final StringBuffer summaryBuffer;
  protected final StringBuffer textBuffer = new StringBuffer();
  protected final Hashtable attributeTable = new Hashtable();
  private StringBuffer currentBuffer;

  public PseudoXmlParser(StringBuffer summaryBuffer) {
    if (summaryBuffer == null) {
      throw new NullPointerException("Summary buffer cannot be null");
    }
    currentBuffer = this.summaryBuffer = summaryBuffer;
  }

  public void chunkParse(InputStream stream) throws IOException {
    resetHandlers();
    summaryBuffer.setLength(0);

    byte[] buff = new byte[1024];
    int l;
    while ((l = stream.read(buff)) != -1) {
      parseInternal(new String(buff, 0, l));
    }
  }

  public void parse(InputStream stream) throws IOException {
    StringBuffer sb = new StringBuffer();
    byte[] buff = new byte[1024];
    int l;
    while ((l = stream.read(buff)) != -1) {
      sb.append(new String(buff, 0, l));
    }
    parse(sb.toString());
  }

  public void parse(String xml) {
    resetHandlers();
    summaryBuffer.setLength(0);

    parseInternal(xml);
  }

  private void parseInternal(String xml) {

    for (int i = 0; i < xml.length(); i++) {
      char c = xml.charAt(i);
      switch (c) {
        case '<':
          if (currentBuffer == valueBuffer || currentBuffer == textBuffer) {
            currentBuffer = endBuffer;
          } else {
            currentBuffer = startBuffer;
          }
          break;
        case '>':
          if (currentBuffer == startBuffer || currentBuffer == valueBuffer) {
            currentBuffer = textBuffer;
            updateAttributes();
          } else if (currentBuffer == endBuffer) {
            currentBuffer = summaryBuffer;
            processElement();
            resetHandlers();
          } else {
            currentBuffer.append(c);
          }
          break;
        case ' ':
          if (currentBuffer == startBuffer) {
            currentBuffer = keyBuffer;
          } else if (currentBuffer == valueBuffer) {
            updateAttributes();
            currentBuffer = keyBuffer;
          } else {
            currentBuffer.append(c);
          }
          break;
        case '=':
          if (currentBuffer == keyBuffer) {
            currentBuffer = valueBuffer;
          } else {
            currentBuffer.append(c);
          }
          break;
        default:
          currentBuffer.append(c);
          break;
      }
    }
  }

  private void processElement() {
    String tag = startBuffer.toString().trim();
    String chars = textBuffer.toString();
    processElement(tag, chars, attributeTable);
  }
  
  protected abstract void processElement(String tag, String chars, Hashtable attributes);

  private void resetHandlers() {
    attributeTable.clear();
    startBuffer.setLength(0);
    endBuffer.setLength(0);
    textBuffer.setLength(0);
    keyBuffer.setLength(0);
    valueBuffer.setLength(0);
  }

  private void updateAttributes() {
    if (keyBuffer.length() > 0) {
      attributeTable.put(keyBuffer.toString(), valueBuffer.toString());
      keyBuffer.setLength(0);
      valueBuffer.setLength(0);
    }
  }
}
