/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.core.util;

/**
 *
 * @author ADIKSONLINE
 */
public class CountUtils {

  public static String formatNumber(long number) {
    StringBuffer sb = new StringBuffer();
    if (number > 99999) {
      sb.append(">").append(99).append("k");
    } else if (number > 999) {
      sb.append(number / 1000).append("k");
    } else {
      sb.append(number);
    }
    return sb.toString();
  }
}
