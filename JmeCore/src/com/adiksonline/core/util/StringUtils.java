/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.core.util;

import java.util.Vector;

/**
 *
 * @author ADIKSONLINE
 */
public class StringUtils {

  private StringUtils() {
  }

  public static String maskString(String str) {
    StringBuffer buf = new StringBuffer(str.length());
    for (int i = 0; i < str.length(); i++) {
      buf.append("*");
    }
    return buf.toString();
  }

  // for JDK1.4 compatibility
  public static String[] split(String str, String delimiter) {
    String[] returnValue;
    int index = str.indexOf(delimiter);
    if (index == -1) {
      returnValue = new String[]{str};
    } else {
      Vector strVector = new Vector();
      int oldIndex = 0;
      while (index != -1) {
        String subStr = str.substring(oldIndex, index);
        strVector.addElement(subStr);
        oldIndex = index + delimiter.length();
        index = str.indexOf(delimiter, oldIndex);
      }
      if (oldIndex != str.length()) {
        strVector.addElement(str.substring(oldIndex));
      }
      //copy to array
      String[] strArray = new String[strVector.size()];
      for (int i = 0; i < strVector.size(); i++) {
        strArray[i] = (String) strVector.elementAt(i);
      }
      returnValue = strArray;
    }

    return returnValue;
  }

  public static String join(long[] arr, String delimiter) {
    StringBuffer buf = new StringBuffer(11 * arr.length);
    for (int i = 0; i < arr.length; i++) {
      if (0 != buf.length()) {
        buf.append(delimiter);
      }
      buf.append(arr[i]);
    }
    return buf.toString();
  }

  public static String join(String[] arr, String delimiter) {
    StringBuffer buf = new StringBuffer(11 * arr.length);
    for (int i = 0; i < arr.length; i++) {
      if (0 != buf.length()) {
        buf.append(delimiter);
      }
      buf.append(arr[i]);
    }
    return buf.toString();
  }

  public static boolean validateEmail(String email) {
    if (email.length() < 5) {
      return false;
    } else if (email.lastIndexOf('@') < 0) {
      return false;
    } else if (email.lastIndexOf('.') <= email.lastIndexOf('@') + 1) {
      return false;
    }
    return true;
  }
}
