/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.core.util;

import java.util.Vector;

/**
 *
 * @author ADIKSONLINE
 */
public class ItemModel {

  private static final int ADDED = 0;
  private static final int REMOVED = 1;
  private static final int CHANGED = 2;
  private static final int CLEARED = 3;
  private boolean dirty;
  private final Vector vector;
  private final Vector updateListeners = new Vector();

  public ItemModel() {
    //TODO: make this class thread safe!
    this(null);
  }

  public ItemModel(Vector v) {
    vector = (v == null) ? new Vector() : v;
  }

  public void addItem(Object o) {
    vector.addElement(o);
    dirty = true;
    broadcastUpdate(ADDED, vector.size() - 1, o);
  }

  public void insertAt(int index, Object o) {
    vector.insertElementAt(o, index);
    dirty = true;
    broadcastUpdate(ADDED, index, o);
  }

  public void setItem(int index, Object o) {
    vector.setElementAt(o, index);
    dirty = true;
    broadcastUpdate(CHANGED, index, o);
  }

  public Object getItem(int index) {
    return vector.elementAt(index);
  }

  public int getIndex(Object o) {
    return vector.indexOf(o);
  }

  public void removeAt(int index) {
    Object o = getItem(index);
    vector.removeElementAt(index);
    dirty = true;
    broadcastUpdate(REMOVED, index, o);
  }

  public void removeItem(Object o) {
    int index = getIndex(o);
    vector.removeElement(o);
    dirty = true;
    broadcastUpdate(REMOVED, index, o);
  }

  public void removeAll() {
    dirty = vector.isEmpty() ? dirty : true;
    vector.removeAllElements();
    broadcastUpdate(CLEARED, -1, null);
  }

  public int getSize() {
    return vector.size();
  }

  public boolean isEmpty() {
    return vector.size() <= 0;
  }

  /**
   * @return the dirty
   */
  public boolean isDirty() {
    return dirty;
  }

  /**
   * sets the dirty flag
   *
   * @param dirty true or false
   */
  public void setDirty(boolean dirty) {
    this.dirty = dirty;
  }

  public void addListener(UpdateListener listener) {
    if (listener == null) {
      throw new IllegalArgumentException("ItemModel listener cannot be null");
    }
    if (!updateListeners.contains(listener)) {
      updateListeners.addElement(listener);
      listener.dataCleared();
      for (int i = 0; i < vector.size(); i++) {
        listener.dataAdded(i, vector.elementAt(i));
      }
    }
  }

  public void removeListener(UpdateListener listener) {
    updateListeners.removeElement(listener);
  }

  private void broadcastUpdate(int type, int index, Object data) {
    for (int i = 0; i < updateListeners.size(); i++) {
      UpdateListener broadcast = (UpdateListener) updateListeners.elementAt(i);
      switch (type) {
        case ADDED:
          broadcast.dataAdded(index, data);
          break;
        case REMOVED:
          broadcast.dataRemoved(index, data);
          break;
        case CLEARED:
          broadcast.dataCleared();
          break;
        case CHANGED:
          broadcast.dataChanged(index, data);
          break;
      }
    }
  }
}
