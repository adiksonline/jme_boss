/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.core.util;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 *
 * @author ADIKSONLINE
 */
public final class QueryMaker {

  private final static String UNRESERVED_CHARS = ".-_0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ*~";
  private static final char[] HEX = "0123456789ABCDEF".toCharArray();
  private final Hashtable queryMap = new Hashtable();

  public QueryMaker() {
  }

  public QueryMaker addQuery(String key, int value) {
    return addQuery(key, Integer.toString(value));
  }

  public QueryMaker addQuery(String key, long value) {
    return addQuery(key, Long.toString(value));
  }

  public QueryMaker addQuery(String key, boolean value) {
    return addQuery(key, (value ? Boolean.TRUE : Boolean.FALSE).toString());
  }

  public QueryMaker addQuery(String key, double value) {
    return addQuery(key, Double.toString(value));
  }

  public QueryMaker addQuery(String key, char value) {
    return addQuery(key, new Character(value).toString());
  }

  public QueryMaker addQuery(String key, Object value) {
    return addQuery(key, value.toString());
  }

  public String urlEncode(final String sUrl) {
    StringBuffer buf = new StringBuffer();
    byte[] bytes;
    try {
      final ByteArrayOutputStream bos = new ByteArrayOutputStream(sUrl.length() * 5 / 4);
      final DataOutputStream dos = new DataOutputStream(bos);
      dos.writeUTF(sUrl);
      bytes = bos.toByteArray();
      dos.close();
    } catch (IOException e) {
      return sUrl;
    }
    if (bytes != null) {
      for (int i = 2; i < bytes.length; i++) {
        byte b = bytes[i];
        if (UNRESERVED_CHARS.indexOf(b) >= 0) {
          buf.append((char) b);
        } else {
          buf.append('%');
          buf.append(HEX[(b >>> 4) & 0x0F]);
          buf.append(HEX[b & 0x0F]);
        }
      }
    }

    return buf.toString();
  }

  public QueryMaker addQuery(String key, String value) {
    if (queryMap.containsKey(key)) {
      queryMap.remove(key);
    }
    queryMap.put(key, value);

    /*
     * queryBuffer.append(urlEncode(key)); queryBuffer.append('=');
     * queryBuffer.append(urlEncode(value));
     queryBuffer.append('&');
     */
    return this;
  }

  public String getEncodedQuery() {
    //queryBuffer.deleteCharAt(queryBuffer.length() - 1);
    //return queryBuffer.toString();
    final StringBuffer queryBuffer = new StringBuffer();
    Enumeration en = queryMap.keys();
    while (en.hasMoreElements()) {
      String key = (String) en.nextElement();
      queryBuffer.append(urlEncode(key));
      queryBuffer.append("=");
      queryBuffer.append(urlEncode((String) queryMap.get(key)));
      if (en.hasMoreElements()) {
        queryBuffer.append("&");
      }
    }
    return queryBuffer.toString();
  }

  public boolean hasKey(String key) {
    return queryMap.containsKey(key);
  }

  public Enumeration getKeys() {
    return queryMap.keys();
  }

  public Object getValue(String key) {
    return queryMap.get(key);
  }

  public String toString() {
    return getEncodedQuery();
  }
}
