/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.core.util;

import java.util.Vector;

/**
 *
 * @author ADIKSONLINE
 */
public final class Cursors {

  private final Vector cursors = new Vector();

  public void reset(String cursor) {
    cursors.removeAllElements();
    cursors.addElement(cursor);
  }

  public void append(String cursor) {
    cursors.addElement(cursor);
  }

  public void appendFirst(String cursor) {
    cursors.insertElementAt(cursor, 0);
  }

  public String get() {
    return (String) cursors.lastElement();
  }

  public String getFirst() {
    if (cursors.isEmpty()) {
      return null;
    }
    return (String) cursors.firstElement();
  }
}
