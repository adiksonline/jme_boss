/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.core.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;

/**
 *
 * @author ADIKSONLINE
 */
public class MultipartRequest {

  private byte[] postBytes = null;
  public static final String BOUNDARY = "bpmultipartdata";

  public MultipartRequest(QueryMaker query, String fileField, String fileName, String fileType, byte[] fileBytes) throws IOException {

    String boundaryMessage = getBoundaryMessage(query, fileField, fileName, fileType);
    String endBoundary = "\r\n--" + BOUNDARY + "--\r\n";
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    bos.write(boundaryMessage.getBytes());
    bos.write(fileBytes);
    bos.write(endBoundary.getBytes());
    this.postBytes = bos.toByteArray();
    bos.close();
  }

  private String getBoundaryMessage(QueryMaker query, String fileField, String fileName, String fileType) {
    StringBuffer sb = new StringBuffer("--").append(BOUNDARY).append("\r\n");
    Enumeration keys = query.getKeys();
    while (keys.hasMoreElements()) {
      String key = (String) keys.nextElement();
      String value = (String) query.getValue(key);

      sb.append("Content-Disposition: form-data; name=\"").append(key).append("\"\r\n")
              .append("\r\n").append(value).append("\r\n")
              .append("--").append(BOUNDARY).append("\r\n");
    }
    sb.append("Content-Disposition: form-data; name=\"").append(fileField)
            .append("\"; filename=\"").append(fileName).append("\"\r\n")
            .append("Content-Type: ").append(fileType).append("\r\n\r\n");
    return sb.toString();
  }

  /**
   * @return the postBytes
   */
  public byte[] getPostBytes() {
    return postBytes;
  }
}
