/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.core.util;

/**
 *
 * @author ADIKSONLINE
 */
public interface UpdateListener {

  public void dataRemoved(int index, Object data);

  public void dataAdded(int index, Object data);

  public void dataChanged(int index, Object data);

  public void dataCleared();
}
