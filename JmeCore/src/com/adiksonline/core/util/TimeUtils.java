/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.core.util;

import com.adiksonline.core.parser.PseudoXmlParser;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.TimeZone;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

/**
 *
 * @author ADIKSONLINE
 */
public class TimeUtils {

  private static final String timeUrl = "http://developer.yahooapis.com/TimeService/V1/getTime?appid=YahooDemo&format=ms";
  private static long offset = 0;
  private static boolean useOffset = false;
  private static boolean syncBusy = false;
  private static final long YEAR = 365 * 24 * 60 * 60;
  private static final long MONTH = 28 * 24 * 60 * 60;
  private static final long DAY = 24 * 60 * 60;
  private static final long HOUR = 60 * 60;
  private static final long MINUTE = 60;

  private static void syncServer() {
    syncBusy = true;
    new Thread(new Runnable() {
      public void run() {
        try {
          HttpConnection con = (HttpConnection) Connector.open(timeUrl, Connector.READ);
          InputStream is = con.openDataInputStream();
          StringBuffer sb = new StringBuffer();
          new PseudoXmlParser(sb) {
            protected void processElement(String tag, String chars, Hashtable attributes) {
              if ("timestamp".equalsIgnoreCase(tag)) {
                summaryBuffer.append(chars);
              }
            }
          }.chunkParse(is);
          long current = System.currentTimeMillis();
          long millis = Long.parseLong(sb.toString().trim());
          offset = getPhoneTime() - getLocalTime(millis) - (System.currentTimeMillis() - current);
          useOffset = true;
          syncBusy = false;
        } catch (Exception ex) {
          offset = 0;
          useOffset = false;
          syncBusy = false;
        }
      }
    }).start();
  }

  private static long getLocalTime(long time) {
    return TimeZone.getDefault().getRawOffset() + time;
  }

  private static String twoDigit(int n) {
    return ((n < 10) ? "0" : "") + n;
  }

  public static String formatRawTime(long time, boolean longFormat) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date(time));

    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH);
    int day = calendar.get(Calendar.DAY_OF_MONTH);

    int hour = calendar.get(Calendar.HOUR);
    if (hour == 0) {
      hour = 12;
    }
    int minute = calendar.get(Calendar.MINUTE);
    String am_pm = (calendar.get(Calendar.AM_PM) == 0) ? "AM" : "PM";

    StringBuffer sb = new StringBuffer();
    if (longFormat) {
      sb.append(twoDigit(day)).append("-").append(twoDigit(month+1)).append("-")
              .append(year).append(" ").append(twoDigit(hour)).append(":")
              .append(twoDigit(minute)).append(" ").append(am_pm);
    } else {
      if (Calendar.getInstance().get(Calendar.YEAR) == year) {
        //we are in that particular year (says the phone), so use dd mmm format
        sb.append(day).append(" ").append(getMonth(month));
      } else {
        //not that same year (says the phone), so use mmm, YY instead
        sb.append(getMonth(month)).append(", ").append(year);
      }
    }

    return sb.toString();
  }

  private static String formatPrettyTime(long time) {
    long difference = (getNowTime() - time) / 1000;

    StringBuffer res = new StringBuffer();
    if (difference >= YEAR) {
      res.append(difference / YEAR).append(" yrs");
    } else if (difference >= MONTH) {
      res.append(difference / MONTH).append(" mnts");
    } else if (difference >= DAY) {
      res.append(difference / DAY).append(" days");
    } else if (difference >= HOUR) {
      res.append(difference / HOUR).append(" hrs");
    } else if (difference >= MINUTE) {
      res.append(difference / MINUTE).append(" mins");
    } else if (difference >= 10) {
      res.append("few secs");
    } else {
      //below 10 seconds counts as just now
      res.append("just now");
    }
    if (res.toString().startsWith("1 ")) {
      //remove trailing s
      res.deleteCharAt(res.length() - 1);
    }
    //you might wanna add "ago" to the time. had to remove it for space reasons :D
    return res.toString();
  }

  public static String formatTime(long millis) {
    long time = getLocalTime(millis);
    if (useOffset) {
      return formatPrettyTime(time);
    } else {
      if (!syncBusy) {
        syncServer();
      }
      return formatRawTime(time, false);
    }
  }

  private static long getPhoneTime() {
    return new Date().getTime();
  }

  private static long getNowTime() {
    return getPhoneTime() - offset;
  }

  private static String getMonth(int month) {
    String res;
    switch (month) {
      case 0:
        res = "Jan";
        break;
      case 1:
        res = "Feb";
        break;
      case 2:
        res = "Mar";
        break;
      case 3:
        res = "Apr";
        break;
      case 4:
        res = "May";
        break;
      case 5:
        res = "Jun";
        break;
      case 6:
        res = "Jul";
        break;
      case 7:
        res = "Aug";
        break;
      case 8:
        res = "Sep";
        break;
      case 9:
        res = "Oct";
        break;
      case 10:
        res = "Nov";
        break;
      case 11:
        res = "Dec";
        break;
      default:
        res = "Unknown";
        break;
    }
    return res;
  }
}
