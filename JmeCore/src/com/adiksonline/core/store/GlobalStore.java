/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adiksonline.core.store;

import java.util.Hashtable;

/**
 *
 * @author ADIKSONLINE
 */
public class GlobalStore {

  public static final String ICONS = "icons";
  public static final String OTHER = "other";
  public static final String USER_ICONS = "user_icons";
  private static final Hashtable globals = new Hashtable();

  static {
    globals.put(ICONS, new Hashtable());
    globals.put(OTHER, new Hashtable());
    globals.put(USER_ICONS, new Hashtable());
  }

  public static Hashtable getTable(String id) {
    if (!globals.containsKey(id)) {
      return null;
    }
    return (Hashtable) globals.get(id);
  }
}
